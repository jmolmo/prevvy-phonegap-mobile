/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {

    // url: 'http://192.168.0.93:8888/prevvy.html#home',
    url: 'https://frontend.develop.prevvy.co/prevvy.html#home',

    browser: undefined,

    options: {
        quality: 100,
        correctOrientation: true,
        // destinationType: Camera.DestinationType.FILE_URI
    },


    // Application Constructor
    initialize: function () {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function () {
        app.receivedEvent('deviceready');
    },

    onClick: function (event) {

        if (!navigator.camera) {
            alert("Camera API not supported", "Error");
            return;
        }

        navigator.camera.getPicture(app.onCameraSuccess, app.onCameraFail, app.options);
    },

    // Update DOM on a Received Event
    receivedEvent: function (id) {
        app.browser = cordova.InAppBrowser.open(this.url, '_blank', 'location=no,zoom=no,hidden=yes,clearsessioncache=yes,clearcache=yes');
        app.browser.addEventListener("loadstart", app.onLoadStart);
        app.browser.addEventListener("loadstop", app.onLoadStop);
    },

    onLoadStart: function (event) {
        app.browser.show();
        $(".m-app-loading").hide();
    },

    query: function (dataType, start, end, healthApi) {
        var vm = this;
        return new Promise(function (resolve, reject) {

            healthApi.queryAggregated({
                    startDate: start,
                    endDate: end,
                    dataType: dataType,
                    bucket: 'day'
                },
                function (data) {
                    resolve({dataType: dataType, data: data});
                },
                function (error) {
                    reject(error);
                });
        });
    },

    /**
     * Begin app
     * @param event
     *
     *
     * com.google.android.apps.fitness
     */
    onLoadStop: function (event) {

        var eventJson = JSON.stringify(event);


        if ( eventJson.includes("GOOGLE-FIT")) {
            app.processGoogleFit();
        }

        /*if (event.url.endsWith('#observation-camera')) {
            app.browser.hide();
            app.browser.executeScript({
                code: "" +
                    "try {" +
                    "   window.prevvyOnOCRHandler('jola')" +
                    "}catch(e){" +
                    "   alert(e)" +
                    "}"
            }, function (e) {
                try {
                    app.browser.show();
                } catch (error) {
                    console.log("Error");
                    console.log(error);
                    console.log("Error");
                }
            });
        } else if (event.url.endsWith('#observation-google-fit')) {

        }*/
    },

    restore: function(viewBrowser) {
        $("#cancelGF").css("display", "none");
        $("#synchronizing").css("display", "none");
        $("#openGF").css("display", "flex");
        $("#connectGF").css("display", "flex");
        $("#skipGF").css("display", "flex");

        if ( viewBrowser ) {
            app.browser.show();
            $(".app").hide();
        } else{
            app.browser.hide();
            $(".app").show();
        }
    },

    requestAuthorization: function(dataTypes){

    },

    /**
     * Request and process info from Google Fit API.
     */
    requestAndProccessData: function() {
        $("#synchronizing").css("display", "flex").text("Getting information ...");

        var start = new Date().getTime() - 3 * 24 * 60 * 60 * 1000;
        var end = new Date();
        var dataTypes = ['steps', 'calories', 'distance'];
        var allPromises = [];

        dataTypes.forEach(function (dataType) {
            allPromises.push(app.query(dataType, start, end, navigator.health));
        });

        Promise.all(allPromises)
            .then(function (result) {
                alert("HOLA")
                $("#synchronizing").css("display", "flex").text("Sincronizando con Prevvy ...");
                $(".app").hide();
                app.browser.show();
                app.browser.executeScript({
                    code: "" +
                        "try {" +
                        "    window.prevvyOnGoogleFitEvent({range: 3, unit: 'd', result: " + JSON.stringify(result) + " })" +
                        "}catch(e){" +
                        "    alert(e)" +
                        "}"
                }, function (e) {
                    try {
                        navigator.health.disconnect(function(a){
                            alert("CHAU");
                            app.restore(true);
                        }, function(error){
                            alert("Error disconnecting");
                        });

                    } catch (error) {
                        navigator.health.disconnect(function(a) {
                            $("#synchronizing").css("display", "flex").text("Fail in connecting with Google");
                            setTimeout(function () {
                                $("#synchronizing").css("display", "none");
                                app.restore(false);
                            }, 3000);
                        });
                    }

                });
            })
            .catch(function (error) {
                window.alert(error);
            });
    },

    /**
     * Connect with Google Fit API
     */
    connectToGoogleFit: function() {
        $("#cancelGF").css("display", "flex");
        $("#synchronizing").css("display", "flex").text("Connecting with Google ...");
        navigator.health.isAuthorized([{read: ['steps', 'calories', 'distance']}], function (isAuthorized) {
            if (!isAuthorized) {
                navigator.health.requestAuthorization([{read: ['steps', 'calories', 'distance']}], function (a) {
                    app.requestAndProccessData();
                }, function (e) {
                    $("#synchronizing").css("display", "flex").text("Fail in connecting with Google");
                    setTimeout(function () {
                        $("#synchronizing").css("display", "none");
                        app.restore(false);
                    }, 3000);
                });
            } else {
                app.requestAndProccessData();
            }
        }, function (error) {
            $("#synchronizing").css("display", "flex").text("Fail in connecting with Google");
            setTimeout(function () {
                $("#synchronizing").css("display", "none");
                app.restore(false);
            }, 3000);
        });
    },

    /**
     * Procesa la informacion con
     */
    processGoogleFit: function () {
        app.browser.hide();
        $(".app").show();
        $("#google-fit").show().css("display", "flex").css("flex-direction", "column");

        /*var select = new MDCSelect(document.querySelector('.mdc-select'));

        select.listen('MDCSelect:change', function(){
            alert("Selected option at index" + select.selectedIndex + " with value " + select.value);
        });*/


        $("#cancelGF").on("click", function () {
            app.restore();
        });


        /**
        * Start Google Fit
        * */
        $("#openGF").on("click", function () {
            try {
                startApp.set({
                    "application": "com.google.android.apps.fitness"
                }).start(function (complete) {
                    // TODO
                }, function (error) {
                    // TODO
                });
           } catch (e) {
                alert(e);
            }
        });


        $("#skipGF").on("click", function () {
            $(".app").hide();
            app.browser.show();
        });


        $("#connectGF").on("click", function () {
            $("#openGF").css("display", "none");
            $("#connectGF").css("display", "none");
            $("#skipGF").css("display", "none");

            try {
                // app.browser.hide();
                navigator.health.isAvailable(function (isAvailable) {
                    if ( !isAvailable ) {
                        navigator.health.promptInstallFit(function(installed) {
                            if ( installed ){
                                app.connectToGoogleFit();
                            } else {
                                alert("Not Google Fit Installed");
                            }
                        }, function(error ) {
                            alert("Not Google Fit Installed");
                        });
                    }else {
                        app.connectToGoogleFit();
                    }
                });
            } catch (error) {
                alert(error);
            }

        });
    },

    onCameraSuccess: function (imgData) {
        textocr.recText(0, 3, imgData, app.onOCRSuccess, app.onOCRFail);
    },

    /*onCameraTesseractSuccess: function(imgData) {
        TesseractPlugin.recognizeText(imgData, "eng", function(recognizedText) {
            $(".textTesseractRecognized").text(recognizedText);
          }, function(reason) {
                alert('Error on recognizing text from image. ' + reason);
          });
    },*/

    onCameraFail: function (error) {
        alert(error, "Error");
    },

    onOCRSuccess: function (text) {

        $(".textRecognized").text(text);


    },

    onOCRFail: function (error) {
        alert(error, "Error");
    }

};
